#!/bin/bash
set -e

if [ -z "$SIGNAL_DEDUP_TARGET" ]
then
  echo "SIGNAL_DEDUP_TARGET not set"
  exit 1
fi

if [ $# -ne 1 ]
then
  echo "First argument should be the signal backup path"
  exit 1
fi

master_json=$(termux-dialog text -t "Master Password" -p)
if [ "$(echo "$master_json"|jq ".code")" != "-1" ]
then
  echo "Invalid master pw"
  exit 1
fi
master_pw="$(echo "$master_json"|jq -r ".text")"

for backup in "$1"/signal-*.backup ; do
  client_json=$(termux-dialog text -t "File Password for $backup" -p)
  if [ "$(echo "$client_json"|jq ".code")" != "-1" ]
  then
    echo "Invalid client pw"
    exit 1
  fi
  client_pw="$(echo "$client_json"|jq -r ".text")"

  SIGNAL_DEDUP_MASTER_PASSWORD="$master_pw" SIGNAL_DEDUP_FILE_PASSWORD="$client_pw" signal_backup_deduplicate "$SIGNAL_DEDUP_TARGET" add "$backup"
  rm "$backup"
done
