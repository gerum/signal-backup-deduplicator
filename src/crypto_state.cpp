#include "crypto_state.h"
#include "crypto.h"
#include "helper.h"

using namespace std::literals;

CryptoState CryptoState::generate(const std::string &password) {
    CryptoState result;

    result.static_info.salt = get_crypto_random_bytes(32);
    result.static_info.iv = get_crypto_random_bytes(12);
    result.static_info.initial_counter = fromBytes<uint32_t>(get_crypto_random_bytes(4));
    result.counter = result.static_info.initial_counter;

    result.fill(password);
    return result;
}

CryptoState CryptoState::fromCryptoConfig(const std::string &password, const signal_backup_incremental::CryptoConfig &config) {
    CryptoState result;

    if (!config.has_salt() || !config.has_iv() || !config.has_initial_counter())
        throw std::runtime_error("invalid config");

    result.static_info.salt = str2byte(config.salt());
    result.static_info.iv = str2byte(config.iv());
    result.static_info.initial_counter = network2Host(config.initial_counter());
    result.counter = result.static_info.initial_counter;

    result.fill(password);
    return result;
}

CryptoState CryptoState::fromSignalHeader(const std::string &password, const std::unique_ptr<signal_app::Header> &header) {
    CryptoState result;

    if (!header->has_salt() || !header->has_iv())
        throw std::runtime_error("invalid config");

    auto [counter_bytes, iv] = split(str2byte(header->iv()), 4);
    result.static_info.salt = str2byte(header->salt());
    result.static_info.iv = std::move(iv);
    result.static_info.initial_counter = network2Host(fromBytes<uint32_t>(counter_bytes));
    result.counter = result.static_info.initial_counter;

    result.fill(password);
    return result;
}

void CryptoState::fill(const std::string &password) {
    auto input = str2byte(password);
    auto hash = input;

    if (!static_info.salt.empty())
        hash = concat(static_info.salt, hash);


    for (size_t i = 0; i < 250'000; i++) {
        hash = sha512(concat(hash, input));
    }
    hash.resize(32);


    std::tie(cipherKey, macKey) = split(hkdf_sha256(hash, "Backup Export"s), 32);
}

std::unique_ptr<signal_backup_incremental::CryptoConfig> CryptoState::toCryptoConfig() const {
    auto result = std::make_unique<signal_backup_incremental::CryptoConfig>();

    result->set_salt(byte2str(static_info.salt));
    result->set_iv(byte2str(static_info.iv));
    result->set_initial_counter(host2Network(static_info.initial_counter));

    return result;
}

std::vector<std::byte> CryptoState::getNextIv() {
    return concat(asBytesView(host2Network(counter++)), static_info.iv);
}
