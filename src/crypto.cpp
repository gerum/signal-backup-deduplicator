#include "crypto.h"
#include <cassert>
#include <cryptopp/aes.h>
#include <cryptopp/filters.h>
#include <cryptopp/hkdf.h>
#include <cryptopp/hmac.h>
#include <cryptopp/modes.h>
#include <cryptopp/osrng.h>
#include <cryptopp/sha.h>

std::vector<std::byte> sha512(const std::span<const std::byte> &data) {
    CryptoPP::SHA512 impl{};

    impl.Update(reinterpret_cast<const CryptoPP::byte *>(data.data()), data.size());

    std::vector<std::byte> r(impl.DigestSize());
    impl.Final(reinterpret_cast<CryptoPP::byte *>(r.data()));
    return r;
}

std::vector<std::byte> hmac_sha256(const std::span<const std::byte> &key, const std::span<const std::byte> &data) {
    CryptoPP::HMAC<CryptoPP::SHA256> impl(reinterpret_cast<const CryptoPP::byte *>(key.data()), key.size());

    impl.Update(reinterpret_cast<const CryptoPP::byte *>(data.data()), data.size());

    std::vector<std::byte> r(impl.DigestSize());
    impl.Final(reinterpret_cast<CryptoPP::byte *>(r.data()));

    return r;
}

std::vector<std::byte> hkdf_sha256(const std::vector<std::byte> &key, const std::string &info) {
    std::vector<std::byte> salt{CryptoPP::SHA256{}.DigestSize()};
    std::vector<std::byte> okm{64};

    CryptoPP::HKDF<CryptoPP::SHA256> hkdf;
    assert(okm.size() <= hkdf.MaxDerivedKeyLength());
    assert(okm.size() >= hkdf.MinDerivedKeyLength());
    hkdf.DeriveKey(reinterpret_cast<CryptoPP::byte *>(okm.data()), okm.size(),
                   reinterpret_cast<const CryptoPP::byte *>(key.data()), key.size(),
                   reinterpret_cast<const CryptoPP::byte *>(salt.data()), salt.size(),
                   reinterpret_cast<const CryptoPP::byte *>(info.data()), info.size());

    return okm;
}

class AES_DEC_IMPL {
    using AES = CryptoPP::CTR_Mode<CryptoPP::AES>::Decryption;
    AES aes{};

public:
    explicit AES_DEC_IMPL(const std::span<const std::byte> &key, const std::span<const std::byte> &iv) {
        aes.SetKeyWithIV(reinterpret_cast<const CryptoPP::byte *>(key.data()), key.size(), reinterpret_cast<const CryptoPP::byte *>(iv.data()), iv.size());
    }

    std::vector<std::byte> decrypt(const std::span<const std::byte> &data) {
        std::vector<std::byte> result(data.size());
        CryptoPP::ArraySource{reinterpret_cast<const CryptoPP::byte *>(data.data()), data.size(), true,
                              new CryptoPP::StreamTransformationFilter{aes,
                                                                       new CryptoPP::ArraySink{reinterpret_cast<CryptoPP::byte *>(result.data()), result.size()}}};
        return result;
    }
};

class AES_ENC_IMPL {
    using AES = CryptoPP::CTR_Mode<CryptoPP::AES>::Encryption;
    AES aes{};

public:
    explicit AES_ENC_IMPL(const std::span<const std::byte> &key, const std::span<const std::byte> &iv) {
        aes.SetKeyWithIV(reinterpret_cast<const CryptoPP::byte *>(key.data()), key.size(), reinterpret_cast<const CryptoPP::byte *>(iv.data()), iv.size());
    }

    std::vector<std::byte> encrypt(const std::span<const std::byte> &data) {
        std::vector<std::byte> result(data.size());
        CryptoPP::ArraySource{reinterpret_cast<const CryptoPP::byte *>(data.data()), data.size(), true,
                              new CryptoPP::StreamTransformationFilter{aes,
                                                                       new CryptoPP::ArraySink{reinterpret_cast<CryptoPP::byte *>(result.data()), result.size()}}};
        return result;
    }
};

AES_DEC::AES_DEC(const std::span<const std::byte> &key, const std::span<const std::byte> &iv) : impl(std::make_unique<AES_DEC_IMPL>(key, iv)) {
}

AES_DEC::~AES_DEC() = default;

std::vector<std::byte> AES_DEC::decrypt(const std::span<const std::byte> &data) {
    return impl->decrypt(data);
}

AES_ENC::AES_ENC(const std::span<const std::byte> &key, const std::span<const std::byte> &iv) : impl(std::make_unique<AES_ENC_IMPL>(key, iv)) {
}

AES_ENC::~AES_ENC() = default;

std::vector<std::byte> AES_ENC::encrypt(const std::span<const std::byte> &data) {
    return impl->encrypt(data);
}

std::vector<std::byte> get_crypto_random_bytes(size_t n) {
    std::vector<std::byte> r(n);
    CryptoPP::AutoSeededRandomPool rng{true};

    rng.GenerateBlock(reinterpret_cast<CryptoPP::byte *>(r.data()), r.size());
    return r;
}
