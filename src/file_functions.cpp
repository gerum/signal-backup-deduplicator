#include "file_functions.h"

std::span<const std::byte> readSizedBlock(MappedInFile &file) {
    size_t size = network2Host(readValue<blocksize_t>(file));
    return file.get(size);
}

void writeSizedBlock(OutFile &file, const std::span<const std::byte> &bytes) {
    file.write(asBytesView(host2Network<blocksize_t>(bytes.size())));
    file.write(bytes);
}

std::vector<std::byte> readBlock_encrypted(MappedInFile &file, CryptoState &h, size_t n) {
    auto full_iv = h.getNextIv();

    auto encrypted_bytes = file.get(n);
    auto their_mac = file.get(mac_size);
    auto our_mac = split(hmac_sha256(h.macKey, concat(full_iv, encrypted_bytes)), 10).first;

    if (!std::ranges::equal(their_mac, our_mac))
        throw std::runtime_error("BAD MAC");

    return AES_DEC(h.cipherKey, full_iv).decrypt(encrypted_bytes);
}

std::vector<std::byte> readSizedBlock_encrypted(MappedInFile &file, CryptoState &h) {
    AES_DEC aes(h.cipherKey, h.getNextIv());

    auto encrypted_size = file.get(sizeof(blocksize_t));
    auto decrypted_size = network2Host(fromBytes<blocksize_t>(aes.decrypt(encrypted_size)));


    if (decrypted_size < mac_size)
        throw std::runtime_error("Block to small for MAC");

    auto frame_size = decrypted_size - mac_size;
    auto encrypted_bytes = file.get(frame_size);

    auto our_mac = truncate(hmac_sha256(h.macKey, concat(encrypted_size, encrypted_bytes)), mac_size);
    auto their_mac = file.get(mac_size);
    if (!std::ranges::equal(their_mac, our_mac))
        throw std::runtime_error("BAD MAC");

    return aes.decrypt(encrypted_bytes);
}

void writeBlock_encrypted(OutFile &file, CryptoState &h, std::span<const std::byte> bytes) {
    auto full_iv = h.getNextIv();

    auto encrypted_bytes = AES_ENC(h.cipherKey, full_iv).encrypt(bytes);
    auto mac = truncate(hmac_sha256(h.macKey, concat(full_iv, encrypted_bytes)), 10);

    file.write(encrypted_bytes);
    file.write(mac);
}

void writeSizedBlock_encrypted(OutFile &file, CryptoState &h, std::span<const std::byte> bytes) {
    AES_ENC aes(h.cipherKey, h.getNextIv());

    auto encrypted_size = aes.encrypt(asBytesView(host2Network<blocksize_t>(mac_size + bytes.size())));
    auto encrypted_bytes = aes.encrypt(bytes);
    auto mac = truncate(hmac_sha256(h.macKey, concat(encrypted_size, encrypted_bytes)), 10);

    file.write(encrypted_size);
    file.write(encrypted_bytes);
    file.write(mac);
}
