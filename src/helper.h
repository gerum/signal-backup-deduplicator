#pragma once
#include "file_wrapper.h"
#include <bit>
#include <cstring>
#include <google/protobuf/message.h>

#pragma clang diagnostic push
#pragma ide diagnostic ignored "Simplify"

template<typename T>
T network2Host(const T &x) {
    if constexpr (std::endian::native == std::endian::big)
        return x;
    else
        return std::byteswap(x);
}

template<typename T>
T host2Network(const T &x) {
    if constexpr (std::endian::native == std::endian::big)
        return x;
    else
        return std::byteswap(x);
}

#pragma clang diagnostic pop

template<std::unsigned_integral T>
std::span<const std::byte> asBytesView(const T &value) {
    return std::as_bytes(std::span(&value, 1));
}

template<std::unsigned_integral T>
T fromBytes(const std::span<const std::byte> &dat) {
    T x{};
    if (dat.size() != sizeof(x))
        throw std::invalid_argument("Bytes not right size");
    std::memcpy(&x, dat.data(), dat.size());
    return x;
}

template<std::derived_from<::google::protobuf::MessageLite> T>
std::unique_ptr<T> fromBytes(const std::span<const std::byte> &dat) {
    auto x = std::make_unique<T>();
    x->ParseFromArray(dat.data(), dat.size());
    return x;
}

template<std::unsigned_integral T>
T readValue(MappedInFile &is) {
    T x{};
    auto dat = is.get(sizeof(x));
    std::memcpy(&x, dat.data(), dat.size());
    return x;
}

std::vector<std::byte> str2byte(const std::string &str);
std::string byte2str(const std::vector<std::byte> &str);

std::vector<std::byte> concat(const std::span<const std::byte> &lhs, const std::span<const std::byte> &rhs);
std::pair<std::vector<std::byte>, std::vector<std::byte>> split(const std::vector<std::byte> &input, size_t n);

std::vector<std::byte> truncate(std::vector<std::byte> &&input, size_t n);
std::vector<std::byte> truncate(std::span<std::byte> input, size_t n);

template<std::derived_from<::google::protobuf::MessageLite> T>
std::vector<std::byte> toBytes(const std::unique_ptr<T> &obj) {
    std::string r;
    obj->SerializeToString(&r);
    return str2byte(r);
}

template<typename T, typename... Types>
constexpr bool holds_ptr_alternative(const std::variant<Types...> &v) noexcept {
    return std::holds_alternative<std::unique_ptr<T>>(v);
}