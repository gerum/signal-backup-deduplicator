#pragma once
#include <cstdint>
#include <memory>
#include <string>
#include <vector>
#include "BackupsIncremental.pb.h"
#include "Backups.pb.h"

struct StaticCryptoState {
    std::vector<std::byte> salt;
    uint32_t initial_counter;
    std::vector<std::byte> iv;
};

struct CryptoState {
    StaticCryptoState static_info;
    std::vector<std::byte> cipherKey;
    std::vector<std::byte> macKey;
    uint32_t counter;

    static CryptoState generate(const std::string &password);
    static CryptoState fromCryptoConfig(const std::string &password, const signal_backup_incremental::CryptoConfig &config);
    static CryptoState fromSignalHeader(const std::string &password, const std::unique_ptr<signal_app::Header> &header);

    [[nodiscard]] std::unique_ptr<signal_backup_incremental::CryptoConfig> toCryptoConfig() const;
    std::vector<std::byte> getNextIv();

private:
    void fill(const std::string &password);
};