#pragma once

#include "Backups.pb.h"
#include "BackupsIncremental.pb.h"
#include "crypto_state.h"
#include "file_wrapper.h"
#include <filesystem>
#include <string>
#include <variant>
#include <vector>

using FrameVariant = std::variant<
        std::unique_ptr<signal_app::Header>,
        std::unique_ptr<signal_app::SqlStatement>,
        std::unique_ptr<signal_app::SharedPreference>,
        std::unique_ptr<signal_app::Attachment>,
        std::unique_ptr<signal_app::DatabaseVersion>,
        std::monostate,
        std::unique_ptr<signal_app::Avatar>,
        std::unique_ptr<signal_app::Sticker>,
        std::unique_ptr<signal_app::KeyValue>>;

FrameVariant decodeFrame(std::unique_ptr<signal_app::BackupFrame> &&frame);

std::pair<CryptoState, std::vector<std::byte>> parseSignalHeader(MappedInFile &file, const std::string &password);

struct ParsedFrame {
    FrameVariant content;
    std::vector<std::byte> decrypted_bytes;
    std::optional<std::vector<std::byte>> additional_data;
};
ParsedFrame parseSignalFrame(MappedInFile &file, CryptoState &h);

