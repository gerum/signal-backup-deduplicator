#include "helper.h"

std::vector<std::byte> str2byte(const std::string &str) {
    std::vector<std::byte> result;
    result.reserve(str.size());
    std::ranges::transform(str, std::back_inserter(result), [](char i) { return std::byte(i); });
    return result;
}

std::string byte2str(const std::vector<std::byte> &str) {
    std::string result;
    result.reserve(str.size());
    std::ranges::transform(str, std::back_inserter(result), [](std::byte i) { return char(i); });
    return result;
}

std::vector<std::byte> concat(const std::span<const std::byte> &lhs, const std::span<const std::byte> &rhs) {
    std::vector<std::byte> r;
    r.reserve(lhs.size());
    r.reserve(rhs.size());

    r.insert(r.end(), lhs.begin(), lhs.end());
    r.insert(r.end(), rhs.begin(), rhs.end());
    return r;
}

std::pair<std::vector<std::byte>, std::vector<std::byte>> split(const std::vector<std::byte> &input, size_t n) {
    if (input.size() < n)
        throw std::out_of_range("Split oor");

    return {std::vector(input.begin(), input.begin() + n), std::vector(input.begin() + n, input.end())};
}

std::vector<std::byte> truncate(std::vector<std::byte> &&input, size_t n) {
    if (n > input.size())
        throw std::runtime_error("Invalid truncate");
    input.resize(n);
    return std::move(input);
}

std::vector<std::byte> truncate(std::span<std::byte> input, size_t n) {
    if (n > input.size())
        throw std::runtime_error("Invalid truncate");
    return {input.begin(), input.begin() + n};
}
