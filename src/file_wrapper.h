#pragma once


#include <filesystem>
#include <fstream>
#include <memory>
#include <span>

class MappedInFile {
    std::span<const std::byte> complete_file_span;
    size_t pos = 0;

public:
    explicit MappedInFile(const std::filesystem::path &path);

    std::span<const std::byte> get(size_t n);
    std::span<const std::byte> getAllRemaining();
    void seek(size_t pos);
};

class OutFile {
    std::ofstream f;

public:
    explicit OutFile(const std::filesystem::path &path);

    virtual void write(const std::span<const std::byte> &data);
    size_t tell();
    void flush();
};