#include "file_wrapper.h"
#include <cstring>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
using namespace std::literals;

MappedInFile::MappedInFile(const std::filesystem::path &path) {
    auto tmp_fd = open(path.c_str(), O_RDONLY);
    if (tmp_fd == -1)
        throw std::runtime_error("File could not be opened: "s + std::strerror(errno));
    auto fd = std::shared_ptr<int>(new int{tmp_fd}, [](const auto &ptr) {
        close(*ptr);
        delete ptr;
    });

    struct stat sb {};

    auto rc = fstat(*fd, &sb);
    if (rc != 0)
        throw std::runtime_error("fstat failed: "s + std::strerror(errno));


    if (sb.st_size != 0) {
        auto ptr = mmap(nullptr, sb.st_size, PROT_READ, MAP_PRIVATE, *fd, 0);
        if (ptr == MAP_FAILED)
            throw std::runtime_error("mmap failed: "s + std::strerror(errno));

        complete_file_span = std::span<std::byte>(reinterpret_cast<std::byte *>(ptr), sb.st_size);
    }
    //TODO: unmap file after use
}

std::span<const std::byte> MappedInFile::get(size_t n) {
    if (pos + n > complete_file_span.size())
        throw std::out_of_range("MappedInFile get of " + std::to_string(n) + " at " + std::to_string(pos));

    auto r = complete_file_span.subspan(pos, n);
    pos += n;
    return r;
}

std::span<const std::byte> MappedInFile::getAllRemaining() {
    return get(complete_file_span.size() - pos);
}

void MappedInFile::seek(size_t p) {
    if (p > complete_file_span.size())
        throw std::out_of_range("MappedInFile seek to " + std::to_string(p));
    pos = p;
}

OutFile::OutFile(const std::filesystem::path &path) {
    std::filesystem::create_directories(path.parent_path());
    f = std::ofstream(path);
    f.exceptions(std::ios_base::badbit | std::ios_base::failbit | std::ios_base::eofbit);
}

void OutFile::write(const std::span<const std::byte> &data) {
    f.write(reinterpret_cast<const char *>(data.data()), data.size());
}

size_t OutFile::tell() {
    return f.tellp();
}

void OutFile::flush() {
    f.flush();
}
