#include "BackupRepo.h"
#include "BackupsIncremental.pb.h"
#include "crypto.h"
#include <iostream>

using namespace std::literals;

std::string get_or_ask(const std::string &env_name, const std::string &question) {
    std::string res;

    if (auto env_ptr = std::getenv(env_name.c_str()))
        return env_ptr;

    std::cout << question << std::endl;
    std::getline(std::cin, res);
    if (!std::cin || res.empty())
        throw std::runtime_error("Could not read " + env_name);
    return res;
}

std::string remove_space(std::string&& input){
    auto erased = std::ranges::remove_if(input, [](char c) { return std::isspace(c); });
    input.erase(erased.begin(), erased.end());
    return std::move(input);
}

int main(int argc, char **argv) {
    std::vector<std::string> args(argv, argv + argc);
#ifdef NDEBUG
    try {
#endif
        std::string master_pw = get_or_ask("SIGNAL_DEDUP_MASTER_PASSWORD", "Enter master password:");

        if (args.size() < 2)
            throw std::runtime_error("Not enough arguments");
        const std::filesystem::path config = args.at(1);
        const std::string cmd = args.at(2);

        BackupRepo backups(config, master_pw);

        if (cmd == "init") {
            if (exists(config))
                throw std::runtime_error("Config does already exists");
            backups.writeToFiles();
            return 0;
        }
        backups.readFromFiles();

        if (cmd == "add") {
            if (args.size() < 3)
                throw std::runtime_error("Not enough arguments");
            std::string backup_pw = remove_space(get_or_ask("SIGNAL_DEDUP_FILE_PASSWORD", "Enter backup file password:"));

            backups.addToBackup(args.at(3), backup_pw);
            backups.writeToFiles();
            return 0;
        } else if (cmd == "list") {
            auto names = backups.get_all_backup_names();
            for (auto &n: names)
                std::cout << n << std::endl;
            return 0;
        } else if (cmd == "restore") {
            if (args.size() < 4)
                throw std::runtime_error("Not enough arguments");
            backups.restoreByName(args.at(4), args.at(3));
            return 0;
        } else {
            throw std::runtime_error("Unknown command");
        }
#ifdef NDEBUG
    } catch (const std::exception &e) {
        std::cerr << "FAILURE: " << e.what() << std::endl;
        std::cout << args.at(0) << " <main_file> init" << std::endl;
        std::cout << args.at(0) << " <main_file> add <signal.backup>" << std::endl;
        std::cout << args.at(0) << " <main_file> list" << std::endl;
        std::cout << args.at(0) << " <main_file> restore <backup_name> <restore_dir>" << std::endl;
        return 1;
    }
#endif
}
