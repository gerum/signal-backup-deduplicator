#pragma once
#include <memory>
#include <span>
#include <string>
#include <vector>

std::vector<std::byte> sha512(const std::span<const std::byte> &data);

std::vector<std::byte> hmac_sha256(const std::span<const std::byte> &key, const std::span<const std::byte> &data);

std::vector<std::byte> hkdf_sha256(const std::vector<std::byte> &key, const std::string &info);

class AES_DEC_IMPL;
class AES_DEC {
    // pimpl like construct to not require any cryptopp types in the header file
    std::unique_ptr<AES_DEC_IMPL> impl;

public:
    explicit AES_DEC(const std::span<const std::byte> &key, const std::span<const std::byte> &iv);
    // Explicit declaration required to allow unique_ptr of forward declared type
    ~AES_DEC();
    std::vector<std::byte> decrypt(const std::span<const std::byte> &data);
};

class AES_ENC_IMPL;
class AES_ENC {
    // pimpl like construct to not require any cryptopp types in the header file
    std::unique_ptr<AES_ENC_IMPL> impl;

public:
    explicit AES_ENC(const std::span<const std::byte> &key, const std::span<const std::byte> &iv);
    // Explicit declaration requried to allow unique_ptr of forware declared type
    ~AES_ENC();
    std::vector<std::byte> encrypt(const std::span<const std::byte> &data);
};

std::vector<std::byte> get_crypto_random_bytes(size_t n);