#pragma once
#include "BackupsIncremental.pb.h"
#include <filesystem>


class BackupRepo {
    struct HashInfo {
        size_t bytes_offset;
        std::vector<std::byte> hash;
    };

    std::filesystem::path config_path;
    std::string password;

    std::unique_ptr<signal_backup_incremental::RepoInfo> repo = std::make_unique<signal_backup_incremental::RepoInfo>();
    std::vector<HashInfo> hash_infos;
    std::map<std::vector<std::byte>, size_t> hash2Info;

    [[nodiscard]] std::filesystem::path dir() const;

private:
    [[nodiscard]] std::unique_ptr<signal_backup_incremental::RepoInfo> read_backup_config() const;
    void write_backup_config() const;
    std::pair<decltype(hash_infos), decltype(hash2Info)> read_backup_metadata();

    std::string addBackupFile(const std::filesystem::path &backup_path, const std::string& file_pw);
    void restore_file(const std::filesystem::path &restore_dir, const signal_backup_incremental::FileInfo &backup_fileinfo);

public:
    BackupRepo(std::filesystem::path file, std::string password);
    void readFromFiles();
    void writeToFiles();
    std::set<std::string> get_all_backup_names();
    void addToBackup(const std::filesystem::path &backup_path, const std::string&file_pw);
    void restoreByName(const std::filesystem::path &d, const std::string &backup_name);
};
