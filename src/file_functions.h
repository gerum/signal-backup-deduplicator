#pragma once
#include "SignalBackupFile.h"
#include "crypto.h"
#include "file_wrapper.h"
#include "helper.h"
#include <concepts>
#include <google/protobuf/message_lite.h>

using blocksize_t = uint32_t;
constexpr size_t mac_size = 10;

// Unencrypted reads

std::span<const std::byte> readSizedBlock(MappedInFile &file);

template<std::derived_from<::google::protobuf::MessageLite> T>
std::unique_ptr<T> readSizedProtobuf(MappedInFile &file) {
    return fromBytes<T>(readSizedBlock(file));
}

// Unencrypted writes

void writeSizedBlock(OutFile &file, const std::span<const std::byte> &bytes);

template<std::derived_from<::google::protobuf::MessageLite> T>
void writeSizedProtobuf(OutFile &file, const std::unique_ptr<T> &obj) {
    writeSizedBlock(file, toBytes(obj));
}

// Encrypted reads

std::vector<std::byte> readBlock_encrypted(MappedInFile &file, CryptoState &h, size_t n);

std::vector<std::byte> readSizedBlock_encrypted(MappedInFile &file, CryptoState &h);

template<std::derived_from<::google::protobuf::MessageLite> T>
std::unique_ptr<T> readSizedProtobuf_encrypted(MappedInFile &file, CryptoState &h) {
    return fromBytes<T>(readSizedBlock_encrypted(file, h));
}

// Encrypted writes

void writeBlock_encrypted(OutFile &file, CryptoState &h, std::span<const std::byte> bytes);

void writeSizedBlock_encrypted(OutFile &file, CryptoState &h, std::span<const std::byte> bytes);

template<std::derived_from<::google::protobuf::MessageLite> T>
void writeSizedProtobuf_encrypted(OutFile &file, CryptoState &h, const std::unique_ptr<T> &obj) {
    writeSizedBlock_encrypted(file, h, toBytes(obj));
}
