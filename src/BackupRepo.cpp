#include "BackupRepo.h"

#include "SignalBackupFile.h"
#include "file_functions.h"
#include "file_wrapper.h"
#include <utility>

std::filesystem::path BackupRepo::dir() const {
    return config_path.parent_path();
}

std::unique_ptr<signal_backup_incremental::RepoInfo> BackupRepo::read_backup_config() const {
    MappedInFile file(config_path);

    auto central_config = readSizedProtobuf<signal_backup_incremental::CryptoConfig>(file);
    auto infos = CryptoState::fromCryptoConfig(password, *central_config);
    return readSizedProtobuf_encrypted<signal_backup_incremental::RepoInfo>(file, infos);
}

void BackupRepo::write_backup_config() const {
    // Does generate new crypto infos to prevent encryption of same plaintext with different counters
    auto infos = CryptoState::generate(password);

    OutFile file(config_path);
    writeSizedProtobuf(file, infos.toCryptoConfig());
    writeSizedProtobuf_encrypted(file, infos, repo);
}

auto BackupRepo::read_backup_metadata() -> std::pair<decltype(hash_infos), decltype(hash2Info)> {
    std::vector<HashInfo> known_hashes;

    for (auto &block: repo->blocks()) {
        if (!block.has_blocks() || !block.has_hashes())
            throw std::runtime_error("Invalid block entry");
        auto &hashes = block.hashes();

        if (!hashes.has_name() || !hashes.has_crypto())
            throw std::runtime_error("Invalid block");

        auto infos = CryptoState::fromCryptoConfig(password, hashes.crypto());

        MappedInFile file(dir() / hashes.name());
        auto hashes_list = readSizedProtobuf_encrypted<signal_backup_incremental::HashFile>(file, infos);

        for (const auto &h: hashes_list->hashes()) {
            if (!h.has_hash() || !h.has_offset())
                throw std::runtime_error("Invalid hash");
            known_hashes.emplace_back(h.offset(), str2byte(h.hash()));
        }
    }

    std::map<std::vector<std::byte>, size_t> inverse_map;
    for (size_t i = 0; i < known_hashes.size(); i++)
        inverse_map[known_hashes[i].hash] = i;

    return {std::move(known_hashes), std::move(inverse_map)};
}

BackupRepo::BackupRepo(std::filesystem::path file, std::string password) : config_path(std::move(file)), password(std::move(password)) {
}

std::string BackupRepo::addBackupFile(const std::filesystem::path &backup_path, const std::string& file_pw) {
    // Ensure that there will never be added two files with the same name (causing restore issues)
    if (get_all_backup_names().contains(backup_path.filename()))
        throw std::runtime_error("Found existing backup with same name");

    // The original backup file to read from
    MappedInFile backup_file(backup_path);
    auto [backup_crypt, original_header] = parseSignalHeader(backup_file, file_pw);

    // The file to write the new data blocks
    auto data_path = (dir() / backup_path.filename()).replace_extension(".data");
    if (exists(data_path))
        throw std::runtime_error("File does already exist");
    OutFile data_file(data_path);
    auto data_crypto = CryptoState::generate(password);

    // The file to write the new data block's hashes
    auto hash_path = (dir() / backup_path.filename()).replace_extension(".hash");
    if (exists(hash_path))
        throw std::runtime_error("File does already exist");
    OutFile hash_file(hash_path);
    auto hash_crypto = CryptoState::generate(password);

    // The file to write the new data block's hashes
    auto inc_backup_path = (dir() / backup_path.filename()).replace_extension(".inc");
    if (exists(inc_backup_path))
        throw std::runtime_error("File does already exist");
    OutFile inc_backup_file(inc_backup_path);
    auto inc_backup_crypto = CryptoState::generate(password);

    // loop
    auto new_hashes = std::make_unique<signal_backup_incremental::HashFile>();
    std::vector<size_t> used_hash_indices;

    while (true) {
        auto [content, decrypted, additional] = parseSignalFrame(backup_file, backup_crypt);

        auto add_single_block = [&](const std::vector<std::byte> &data) {
            auto hash = sha512(data);
            auto it = hash2Info.find(hash);

            if (it == hash2Info.end()) {
                auto &ph = *new_hashes->add_hashes();
                ph.set_hash(byte2str(hash));
                ph.set_offset(data_file.tell());

                it = hash2Info.emplace(std::move(hash), hash_infos.size()).first;
                hash_infos.emplace_back(data_file.tell(), hash);

                writeSizedBlock_encrypted(data_file, data_crypto, data);
            }
            used_hash_indices.push_back(it->second);
        };

        add_single_block(decrypted);
        // Store the appended file data separate to allow deduplication even if metainformation is not consistent
        if (additional)
            add_single_block(*additional);

        if (std::holds_alternative<std::monostate>(content))
            break;
    }

    // Write the hashes of the new blocks to file
    writeSizedProtobuf_encrypted(hash_file, hash_crypto, new_hashes);

    // Update repo, with the newly known blocks
    {
        auto &entry = *repo->mutable_blocks()->Add();
        auto &b = *entry.mutable_blocks();
        auto &h = *entry.mutable_hashes();
        entry.set_num_entries(new_hashes->hashes_size());

        h.set_name(hash_path.filename().string());
        b.set_name(data_path.filename().string());
        h.set_allocated_crypto(hash_crypto.toCryptoConfig().release());
        b.set_allocated_crypto(data_crypto.toCryptoConfig().release());
    }


    // Write the backup info file
    auto backup_info = std::make_unique<signal_backup_incremental::BackupInfo>();
    backup_info->set_original_name(backup_path.filename());
    backup_info->set_original_header(byte2str(original_header));
    backup_info->set_allocated_original_crypto(backup_crypt.toCryptoConfig().release());
    backup_file.seek(0);
    backup_info->set_original_hash(byte2str(sha512(backup_file.getAllRemaining())));
    backup_info->set_original_pw(file_pw);

    // Converting the plain indices to ranges of consecutive indices, to save storage
    std::vector<std::pair<size_t, size_t>> index_ranges;
    if (!used_hash_indices.empty()) {
        index_ranges.emplace_back(used_hash_indices.front(), used_hash_indices.front() + 1);

        for (auto it = std::next(used_hash_indices.begin()); it != used_hash_indices.end(); ++it) {
            if (index_ranges.back().second == *it) {
                index_ranges.back().second++;
            } else {
                index_ranges.emplace_back(*it, *it + 1);
            }
        }
    }
    for (auto [b, e]: index_ranges) {
        auto x = backup_info->mutable_hashes()->Add();
        x->set_begin_index(b);
        x->set_end_index(e);
    }
    writeSizedProtobuf_encrypted(inc_backup_file, inc_backup_crypto, backup_info);


    // Update repo, with the newly known backup
    {
        auto &entry = *repo->mutable_backups()->Add();
        entry.set_name(inc_backup_path.filename().string());
        entry.set_allocated_crypto(inc_backup_crypto.toCryptoConfig().release());
    }
    return inc_backup_path.filename().string();
}

void BackupRepo::restore_file(const std::filesystem::path &restore_dir, const signal_backup_incremental::FileInfo &backup_fileinfo) {
    MappedInFile info_file(dir() / backup_fileinfo.name());
    auto info_crypto = CryptoState::fromCryptoConfig(password, backup_fileinfo.crypto());
    auto backup_info = readSizedProtobuf_encrypted<signal_backup_incremental::BackupInfo>(info_file, info_crypto);

    OutFile restored(restore_dir / backup_info->original_name());
    auto restored_crypto = CryptoState::fromCryptoConfig(backup_info->original_pw(), backup_info->original_crypto());
    writeSizedBlock(restored, str2byte(backup_info->original_header()));

    // Unpack the index ranges to plain indices
    std::vector<size_t> hashes;
    for (auto &p: backup_info->hashes()) {
        for (size_t i = p.begin_index(); i < p.end_index(); i++)
            hashes.push_back(i);
    }

    struct FileContext {
        MappedInFile file;
        CryptoState crypto{};
    };

    /// Transparent comparator allows to compare ranges with indices. Ranges that overlap are incomparable.
    struct Comparator {
        using is_transparent = void;

        bool operator()(const std::pair<size_t, size_t> &lhs, const std::pair<size_t, size_t> &rhs) const {
            if (lhs.second <= rhs.first)
                return true;
            else if (rhs.second <= lhs.first)
                return false;
            else
                throw std::runtime_error("Uncomparable");
        }

        bool operator()(const std::pair<size_t, size_t> &lhs, size_t rhs) const {
            return lhs.second <= rhs;
        }

        bool operator()(size_t lhs, const std::pair<size_t, size_t> &rhs) const {
            return lhs < rhs.first;
        }
    };

    // Open every incremental data file to be able to access all blocks
    std::map<std::pair<size_t, size_t>, FileContext, Comparator> block_files;
    size_t last_index = 0;
    for (auto &b: repo->blocks()) {
        block_files.emplace(std::piecewise_construct,
                            std::forward_as_tuple(last_index, last_index + b.num_entries()),
                            std::forward_as_tuple(MappedInFile(dir() / b.blocks().name()), CryptoState::fromCryptoConfig(password, b.blocks().crypto())));
        last_index = last_index + b.num_entries();
    }

    // This flag is required to determine if a normal block or raw data is restored
    bool sized_flag = true;
    for (auto hash_index: hashes) {
        auto block_file_info_it = block_files.find(hash_index);
        if (block_file_info_it == block_files.end())
            throw std::runtime_error("Invalid block id");
        auto &block_file_range = block_file_info_it->first;
        auto &block_file_info = block_file_info_it->second;

        block_file_info.crypto.counter = block_file_info.crypto.static_info.initial_counter + (hash_index - block_file_range.first);
        block_file_info.file.seek(hash_infos.at(hash_index).bytes_offset);

        auto block_bytes = readSizedBlock_encrypted(block_file_info.file, block_file_info.crypto);

        if (sized_flag) {
            writeSizedBlock_encrypted(restored, restored_crypto, block_bytes);
            auto f = decodeFrame(fromBytes<signal_app::BackupFrame>(block_bytes));
            // These types of frames are always followed by a raw data block
            if (holds_ptr_alternative<signal_app::Attachment>(f) || holds_ptr_alternative<signal_app::Sticker>(f) || holds_ptr_alternative<signal_app::Avatar>(f))
                sized_flag = false;
        } else {
            writeBlock_encrypted(restored, restored_crypto, block_bytes);
            sized_flag = true;
        }
    }

    restored.flush();
    // Compare if the restore was successful and has restore the exact same file
    auto new_hash = sha512(MappedInFile(restore_dir / backup_info->original_name()).getAllRemaining());
    if (!std::ranges::equal(new_hash, str2byte(backup_info->original_hash()))) {
        std::filesystem::remove(restore_dir / backup_info->original_name());
        throw std::runtime_error("Restore not successful");
    }
}

void BackupRepo::readFromFiles() {
    if (!exists(config_path))
        throw std::runtime_error("Backup main file does not exist");
    repo = read_backup_config();
    std::tie(hash_infos, hash2Info) = read_backup_metadata();
}

void BackupRepo::writeToFiles() {
    write_backup_config();
}

void BackupRepo::restoreByName(const std::filesystem::path &d, const std::string &backup_name) {
    for (const auto &b: repo->backups())
        if (std::filesystem::path(b.name()).replace_extension("") == backup_name)
            return restore_file(d, b);
    throw std::runtime_error("Backup not found");
}

std::set<std::string> BackupRepo::get_all_backup_names() {
    std::set<std::string> r;
    for (const auto &b: repo->backups())
        r.emplace(std::filesystem::path(b.name()).replace_extension("").string());
    return r;
}

void BackupRepo::addToBackup(const std::filesystem::path &backup_path, const std::string&file_pw) {
    auto name = addBackupFile(backup_path, file_pw);

    struct TempFolder {
        std::filesystem::path path;

        explicit TempFolder(const std::filesystem::path &d) : path(d / "temp") {
            std::filesystem::create_directories(path);
        }

        ~TempFolder() {
            std::filesystem::remove_all(path);
        }
    } t_d(dir());

    restoreByName(t_d.path, std::filesystem::path(name).replace_extension("").string());
}
