#include "SignalBackupFile.h"
#include "Backups.pb.h"
#include "crypto.h"
#include "file_functions.h"
#include "helper.h"

using namespace std::literals;

FrameVariant decodeFrame(std::unique_ptr<signal_app::BackupFrame> &&frame) {
    using testFunktion = bool (signal_app::BackupFrame::*)() const;
    using extractFunktion = FrameVariant (*)(signal_app::BackupFrame &&);

    const static std::vector<std::tuple<testFunktion, extractFunktion>> function_map = {
            {&signal_app::BackupFrame::has_header, [](signal_app::BackupFrame &&frame) { return FrameVariant{std::unique_ptr<signal_app::Header>(frame.release_header())}; }},
            {&signal_app::BackupFrame::has_statement, [](signal_app::BackupFrame &&frame) { return FrameVariant{std::unique_ptr<signal_app::SqlStatement>(frame.release_statement())}; }},
            {&signal_app::BackupFrame::has_preference, [](signal_app::BackupFrame &&frame) { return FrameVariant{std::unique_ptr<signal_app::SharedPreference>(frame.release_preference())}; }},
            {&signal_app::BackupFrame::has_attachment, [](signal_app::BackupFrame &&frame) { return FrameVariant{std::unique_ptr<signal_app::Attachment>(frame.release_attachment())}; }},
            {&signal_app::BackupFrame::has_version, [](signal_app::BackupFrame &&frame) { return FrameVariant{std::unique_ptr<signal_app::DatabaseVersion>(frame.release_version())}; }},
            {&signal_app::BackupFrame::has_end, [](signal_app::BackupFrame &&frame) {     if(!frame.end()) throw std::runtime_error("End flag not set"); return FrameVariant{std::monostate{}}; }},
            {&signal_app::BackupFrame::has_avatar, [](signal_app::BackupFrame &&frame) { return FrameVariant{std::unique_ptr<signal_app::Avatar>(frame.release_avatar())}; }},
            {&signal_app::BackupFrame::has_sticker, [](signal_app::BackupFrame &&frame) { return FrameVariant{std::unique_ptr<signal_app::Sticker>(frame.release_sticker())}; }},
            {&signal_app::BackupFrame::has_keyvalue, [](signal_app::BackupFrame &&frame) { return FrameVariant{std::unique_ptr<signal_app::KeyValue>(frame.release_keyvalue())}; }}};

    extractFunktion ext = nullptr;
    for (auto &[has, parse]: function_map) {
        if (((*frame).*has)()) {
            if (ext != nullptr)
                throw std::runtime_error("Frame with to many sub-objects");
            ext = parse;
        }
    }

    if (ext == nullptr)
        throw std::runtime_error("Frame without any sub-object");

    return ext(std::move(*frame));
}

std::pair<CryptoState, std::vector<std::byte>> parseSignalHeader(MappedInFile &file, const std::string &password) {
    auto frame_bytes = readSizedBlock(file);
    auto headerFrame = decodeFrame(fromBytes<signal_app::BackupFrame>(frame_bytes));

    if (!holds_ptr_alternative<signal_app::Header>(headerFrame))
        throw std::runtime_error("File does not start with Header");
    auto &header = std::get<std::unique_ptr<signal_app::Header>>(headerFrame);

    if (!header->has_iv())
        throw std::runtime_error("Header has no IV");
    if (!header->has_version())
        throw std::runtime_error("Header has no version");
    if (header->version() != 1)
        throw std::runtime_error("Header has not version=1");
    if (!header->has_salt())
        throw std::runtime_error("Header has no salt");

    auto result = CryptoState::fromSignalHeader(password, header);

    return {result, std::vector<std::byte>(frame_bytes.begin(), frame_bytes.end())};
}

ParsedFrame parseSignalFrame(MappedInFile &file, CryptoState &h) {
    auto decrypted_bytes = readSizedBlock_encrypted(file, h);
    auto frame = decodeFrame(fromBytes<signal_app::BackupFrame>(decrypted_bytes));

    auto add = std::visit([&file, &h](const auto &f) -> std::optional<std::vector<std::byte>> {
        constexpr bool attachment = std::is_same_v<std::remove_cvref_t<decltype(f)>, std::unique_ptr<signal_app::Attachment>>;
        constexpr bool sticker = std::is_same_v<std::remove_cvref_t<decltype(f)>, std::unique_ptr<signal_app::Sticker>>;
        constexpr bool avatar = std::is_same_v<std::remove_cvref_t<decltype(f)>, std::unique_ptr<signal_app::Avatar>>;

        if constexpr (attachment || sticker || avatar) {
            return readBlock_encrypted(file, h, f->length());
        } else
            return {};
    },
                          frame);

    return {std::move(frame), decrypted_bytes, add};
}
