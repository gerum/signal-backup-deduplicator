# Signal Backup Deduplicator

**DISCLAIMER: I use this program myself,
but I will not give any guarantee that no data will be lost!!!**

## Problem

[Signal](https://github.com/signalapp/) backups are too big.

As Signal will only do full backups instead on some kind of incremental ones and their size can be multiple gigabytes,
it is very inconvenient to work with them.
Uploading a signal backup to a server can be quite time-consuming and if you want to store multiple versions,
the total amount of required storage can sum up quickly.

## Solution

As the backups are internally already separated into small blocks and most blocks will remain identical in multiple
backup, it is possible to store only new blocks and the information which blocks belong to a specific backup.
To be able to do this, the original backup files must be decrypted to allow the deduplication on the plaintext data.
There are two reasons why the deduplication can not be done on the encrypted backups:

1. Each backup is encrypted with a new key, so the blocks will be different even if the plaintext is the same.
2. On the encrypted backup it is no possible to identify where a block ends.

## Usage

All commands require the root password that is used to encrypt the backup set.
This can either be passed in via stdin or via the environment variable `SIGNAL_DEDUP_MASTER_PASSWORD`.
If the environment variable is set it will be used, otherwise there will be an interactive prompt for the password.

### cmd arguments

- `<path/to/main/file> init`:
  Creates the file with the central management information.
  The file should be in its own folder, because the latter added backup files are placed automatically in the same
  folder.
  This must be done before any other command can be used.
- `<path/to/main/file> add <path/to/existing/signal.backup>`:
  This command adds the given backup file to deduplicated backup set.
  This command does additionally need the signal backup password to decrypt the backup file.
  The mechanism is the same as for the master password and the corresponding variable is
  called `SIGNAL_DEDUP_FILE_PASSWORD`.
  For the first backup this will not save any storage but following backup should be much smaller.
- `<path/to/main/file> list`:
  Prints a list with all backups that are includes in the backup set.
- `<path/to/main/file> restore <backup_name> <dir/for/restore/>`:
  With this command the original backup file can be restored.
  This command does not take a filename to restore or a password for the restored file,
  because both information are stored beside the deduplicated backup to allow to restore the identical file as was put
  in.